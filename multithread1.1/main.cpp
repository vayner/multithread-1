#include <omp.h>
#include <stdlib.h>

#include <random>
#include <chrono>
#include <vector>
#include <string>

const int iterations = 20000000;

std::random_device rd;
std::default_random_engine re(rd());
std::uniform_real_distribution<double> rnd(0.0, 1.0);

double a[iterations];
double b[iterations];

/**
 * @fn  long long criticalSector()
 *
 * @brief   OpenMP critical sector time test.
 *
 * @return  The time used in microseconds.
 */

long long criticalSector();

/**
 * @fn  long long atomicSector()
 *
 * @brief   OpenMP atomic sector time test.
 *
 * @return  The time used in microseconds.
 */

long long atomicSector();

/**
 * @fn  long long reduction()
 *
 * @brief   OpenMP reduction time test.
 *
 * @return  The time used in microseconds.
 */

long long reduction();

/**
 * @fn  void initRandomValues();
 *
 * @brief   Initializes the random values.
 */

void initRandomValues();

/**
 * @fn  int main()
 *
 * @brief   Main entry-point for this application.
 *
 * @return  Exit-code for the process - 0 for success, else an error code.
 */

int main() {

    initRandomValues();
    
    long long reductionTime = reduction();
    long long atomicTime = atomicSector();
    long long criticalTime = criticalSector();

    printf("Reduction: %12lld\nAtomic:    %12lld\nCritical:  %12lld\n",
        reductionTime,
        atomicTime,
        criticalTime
    );

    system("PAUSE");

    return 0;
}

long long criticalSector() {
    int hits = 0;

    auto before = std::chrono::high_resolution_clock::now();

#pragma omp parallel for
    for (int i = 0; i < iterations; i++) {
        if (a[i] * a[i] + b[i] * b[i] <= 1.) {
#pragma omp critical
            ++hits;
        }
    }

    auto after = std::chrono::high_resolution_clock::now();
    auto diff = after - before;

    return std::chrono::duration_cast<std::chrono::microseconds>(diff).count();
}

long long atomicSector() {
    int hits = 0;

    auto before = std::chrono::high_resolution_clock::now();

#pragma omp parallel for
    for (int i = 0; i < iterations; i++) {
        if (a[i] * a[i] + b[i] * b[i] <= 1.) {
#pragma omp atomic
            ++hits;
        }
    }

    auto after = std::chrono::high_resolution_clock::now();
    auto diff = after - before;

    return std::chrono::duration_cast<std::chrono::microseconds>(diff).count();
}

long long reduction() {
    int hits = 0;

    auto before = std::chrono::high_resolution_clock::now();

#pragma omp parallel for reduction(+:hits)
    for (int i = 0; i < iterations; i++) {
        if (a[i] * a[i] + b[i] * b[i] <= 1.) {
            ++hits;
        }
    }

    auto after = std::chrono::high_resolution_clock::now();
    auto diff = after - before;

    double approx = ((double) hits / (double) iterations) * 4.;

    printf("Aproximated Pi to: %lf\n", approx);

    return std::chrono::duration_cast<std::chrono::microseconds>(diff).count();
}

void initRandomValues() {
    auto rndGenBegin = std::chrono::high_resolution_clock::now();
#pragma omp parallel for
    for (int i = 0; i < iterations; i++) {
        a[i] = rnd(re);
        b[i] = rnd(re);
    }

    auto rndGenEnd = std::chrono::high_resolution_clock::now();
    auto gendiff = rndGenEnd - rndGenBegin;

    long long microDiff = std::chrono::duration_cast<std::chrono::microseconds>(gendiff).count();

    printf("Random took: %i microseconds\n", microDiff);
}