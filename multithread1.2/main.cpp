#include <omp.h>
#include <stdlib.h>

#include <random>
#include <chrono>
#include <thread>
#include <mutex>
#include <clocale>
#include <array>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>

std::random_device rd;
std::default_random_engine re(rd());
std::uniform_real_distribution<double> rnd(0.0, 1.0);

const int ITERATIONS = 1000;
double choice[ITERATIONS];

const int MIN_THREADS = 1;
const int MAX_THREADS = 100;

const int RUNS = 10;
const int REPETITIONS = 5;

const std::array<double, 5> THRESHOLD_LIST = {
    0.9,
    0.7,
    0.5,
    0.3,
    0.1
};

/**
 * @fn  inline void loadbar(unsigned int current, unsigned int end, bool force = false, unsigned int w = 50U);
 *
 * @brief   Loadbars.
 *
 * @param   current The current value.
 * @param   end     The end value.
 * @param   force   (Optional) true to force update.
 * @param   width   (Optional) the width of the progress bar.
 */

inline void loadbar(unsigned int current, unsigned int end, bool force = false, unsigned int width = 50U);

/**
 * @fn  void doWork();
 *
 * @brief   Executes some random fake work.
 */

void doWork();

/**
 * @fn  void initRandomValues();
 *
 * @brief   Initializes the random values.
 */

void initRandomValues();

/**
 * @fn  long long parallelWork(double threshold, int threads);
 *
 * @brief   Do random parallel work.
 *
 * @param   threshold   The threshold for the work to be critical.
 * @param   threads     The number of threads to use.
 *
 * @return  A long.
 */

long long parallelWork(double threshold, int threads);

/**
 * @fn  int main()
 *
 * @brief   Main entry-point for this application.
 *
 * @return  Exit-code for the process - 0 for success, else an error code.
 */

int main() {
    std::setlocale(LC_NUMERIC, "");

    for (int run = 1; run <= RUNS; run++) {
        initRandomValues();
        printf("Run: %d\n", run);
        std::vector<std::vector<long long>> resultList;

        for (const double &threshold : THRESHOLD_LIST) {
            std::vector<long long> data;

            printf("%f\n", threshold);

            loadbar(0, MAX_THREADS);

            for (size_t i = MIN_THREADS; i <= MAX_THREADS; i++) {
                long long result = LLONG_MAX;
                long long intermidiate;

                for (size_t j = 0; j < REPETITIONS; j++) {
                    intermidiate = parallelWork(threshold, i);

                    result = (result > intermidiate) ? intermidiate : result;
                }

                loadbar(i, MAX_THREADS);
                data.push_back(result);
            }

            printf("\n");

            resultList.push_back(data);
        }

        printf("\n");

        std::stringstream filename;

        filename << "RUN_" << std::setw(std::to_string(RUNS).length()) << std::setfill('0') << run << ".csv";

        std::ofstream out(filename.str());

        if (out.good()) {
            for (std::vector<long long> &list : resultList) {
                for (long long &res : list) {
                    out << res << '\t';
                }

                out << '\n';
            }
        } else {
            printf("FAILED TO OPEN FILE %s\n", filename.str());
        }
    }

    return 0;
}

long long parallelWork(double threshold, int threads) {
    std::mutex mutex;
    auto before = std::chrono::high_resolution_clock::now();

    omp_set_num_threads(threads);

#pragma omp parallel for
    for (int i = 0; i < ITERATIONS; i++) {
        if (choice[i] > threshold) {
            std::lock_guard<std::mutex>guard(mutex);
            doWork();
        } else {
            doWork();
        }
    }

    auto after = std::chrono::high_resolution_clock::now();
    auto diff = after - before;

    return std::chrono::duration_cast<std::chrono::microseconds>(diff).count();
}

void doWork() {
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
}

void initRandomValues() {
    auto rndGenBegin = std::chrono::high_resolution_clock::now();
#pragma omp parallel for
    for (int i = 0; i < ITERATIONS; i++) {
        choice[i] = rnd(re);
    }

    auto rndGenEnd = std::chrono::high_resolution_clock::now();
    auto gendiff = rndGenEnd - rndGenBegin;

    long long microDiff = std::chrono::duration_cast<std::chrono::microseconds>(gendiff).count();

    printf("Random took: %i microseconds\n", microDiff);
}

// Based on http://www.rosshemsley.co.uk/2011/02/creating-a-progress-bar-in-c-or-any-other-console-app/
inline void loadbar(unsigned int current, unsigned int end, bool force, unsigned int width) {

    if ((current != end) && (current % ((end / 100) + 1) != 0) && !force) return;

    float ratio = current / (float) end;
    int   progress = ratio * width;

    //printf(" %03d%% [%*c%*c\r", (int) (ratio * 100), progress, '#', width - progress, ']');
    std::cout << " " << std::setw(3) << std::setfill('0') << (int) (ratio * 100) << "% ["
        << std::setw(progress) << std::setfill('=') <<  ((progress) ? "#" : "")
        << std::setw(width - progress) << std::setfill(' ') << ']' << '\r' << std::flush;
}